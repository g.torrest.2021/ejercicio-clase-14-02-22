#Ejercicios OOP Programación en entornos
#Ejercicio 1
from logging import raiseExceptions


class Cuenta:
    def __init__(self,n,s):
        self.nombre=n
        self.saldo=s
    def __str__(self):
        return "el empleado {name} presenta {saldo}".format(name=self.nombre,saldo=self.saldo)
    def ingresos(self,extra):
        aumento=self.saldo+extra
        self.saldo=aumento
        mensaje= self.__str__()
        return mensaje
    def reintegro(self,deuda):
        reintegro=self.saldo - deuda
        self.saldo=reintegro
        mensaje= self.__str__()
        return mensaje
    def transferencia(self,c_2,imp):
        traspaso=self.saldo-imp
        self.saldo=traspaso
        transf=c_2.ingresos(imp)
        mensaje=(self.__str__(), transf.__str__())
        return mensaje
p_1=Cuenta("Juan",25000)
p_2=Cuenta("Marcos", 1755)
ing_1=p_1.ingresos(2500)
print(ing_1)
rein_1=p_1.reintegro(1400)
print(rein_1)
trans_1=p_1.transferencia(p_2,500)
print(trans_1)
ing_2=p_2.ingresos(2500)
print(ing_2)
