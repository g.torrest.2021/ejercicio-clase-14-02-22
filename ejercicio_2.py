#Ejercicios OOP Programación en entornos
#Ejercicio 2
class Contador:
    def __init__(self,v):
        self.valor=v
    def __str__(self):
        return "El valor del  contador es {val}".format(val=self.valor)
    def incremento(self,aum):
        aumento=self.valor+aum
        self.valor=aumento
        return self.__str__()
    def disminuyo(self,aum):
        disminucion=self.valor-aum
        self.valor=disminucion
        return self.__str__()
ejemplo=Contador(0)
i_1=ejemplo.incremento(45)
d_1=ejemplo.disminuyo(5)

#Ejericio 3
#en tun atributo ___atributo hace que el atributo no sea visible ni modificable
class Libro:
    def __init__(self,n,e):
        self.nombre=n
        self.estado=e
    def __str__(self):
        mensaje= "EL libro {name} presenta el siguiente estado en la biblioteca {state}".format(name=self.nombre,state=self.estado)
        return mensaje
    def prestame(self):
        est="El libro se encuentra prestado"
        self.estado=est
        return self
    def devolución(self):
        est="El libro ha sido devuelto"
        self.estado=est
        return self
libro_2=Libro("La sirenita", "Disponible")

#Ejercicio 4
class Fracción:
    def __init__(self,num,denom):
        self.numerador= num
        self.denominador= denom
        
    def multiplica(self,f_2):
        num_p=self.numerador*f_2.numerador
        denom_p=self.denominador*f_2.denominador
        resultado=Fracción(num_p,denom_p)
        return resultado
    def divide(self,f_2):
        num_p=self.numerador*f_2.denominador
        denom_p=self.denominador*f_2.numerador
        resultado=Fracción(num_p,denom_p)
        return resultado
    def sumame_comun_den(self,f_2):
        suma_num= self.numerador+f_2.denominador
        denom= self.denominador
        resultado= Fracción(suma_num,denom)
        return resultado
    def sumame_dif_denom():
"""notas:buena practica separar fichero de clases del de pruebas\
    #No emplear imputs ni prints en métodos de una clase\
    # #sobrescritura:crea un objeto a partir del constructor del padre y añade elementos\
    # si me cuesta pensar un valor por defecto es probable que no exista
    # con un setter tambien puedes añadir excepciones a cambios que conoces que no \
    puedes modificar"""