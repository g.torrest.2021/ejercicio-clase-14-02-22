class Fecha:
    def __init__(self,d=1,m=1,a=2022):
        self.__dia=d
        self.__mes=m
        self.__años=a
    def get_dia(self):
        return self.__dia
    def set_dia(self,day):
        self.__dia=day
    def get_mes(self):
        return self.__mes
    def set_mes(self,month):
        self.__mes=month
    def get_año(self):
        return self.__dia
    def set_año(self,year):
        self.__años=year
    def __str__(self):
        return "la fecha es {dia}/{mes}/{año}".format(dia=self.__dia,mes=self.__mes,año=self.__años)
    
"""Programa Pruebas"""
hoy=Fecha(31,5,2003)
ver_día=hoy.get_dia()
ver_mes=hoy.get_mes()
ver_año=hoy.get_año()
mañana=Fecha()
dia_man=mañana.set_dia(1)
mes_man=mañana.set_mes(6)
año_man=mañana.set_año(2003)
print(hoy)
print(ver_día)
print(ver_mes)
print(mañana)